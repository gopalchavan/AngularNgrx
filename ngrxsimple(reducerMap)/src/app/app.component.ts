import { Component } from '@angular/core';
import {Store} from '@ngrx/store';

import * as RootReducer from './store/reducers'
import { Observable } from 'rxjs/Observable';
import { rootRenderNodes } from '@angular/core/src/view/util';
import { Subscription } from 'rxjs/Subscription';

import * as PostActions from './store/actions/post.action';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  post$:any;
  result:any;
  text:string;
  constructor(private _store:Store<RootReducer.AppState>){    
    this.post$ = this._store.select(RootReducer.getPostState)  
  }
  editText() {
    this._store.dispatch(new PostActions.EditPostAction(this.text) )
  }
  resetPost() {
    this._store.dispatch(new PostActions.Reset())
  }
  upvote() {
    this._store.dispatch(new PostActions.LikeAction())
  }
  downvote() {
    this._store.dispatch(new PostActions.DislikeAction())
  }
}
