import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {reducers,getInitialState} from './store/reducers';

import { AppComponent } from './app.component';
import {StoreModule} from '@ngrx/store';

import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    StoreModule.forRoot(reducers,{
      initialState: getInitialState
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
