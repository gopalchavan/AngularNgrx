import {Action} from '@ngrx/store';
import {Post} from '../models/post';

export const EDIT_POST =    '[Post] Edit';
export const LIKE =         '[Post] Like';
export const DISLIKE =      '[Post] Dislike';
export const RESET =        '[Post] Reset';

export class EditPostAction implements Action{
    readonly type = EDIT_POST;
    constructor(public payload: string){}
}
export class LikeAction implements Action{
    readonly type = LIKE;
}
export class DislikeAction implements Action{
    readonly type = DISLIKE;
}
export class Reset implements Action{
    readonly type = RESET;
}

export type All
    = EditPostAction
    | LikeAction
    | DislikeAction
    | Reset;