import { ActionReducer, ActionReducerMap} from '@ngrx/store';


import * as fromPost from './post.reducer';

export interface AppState {
    postState: fromPost.State;    
}

export const reducers: ActionReducerMap<AppState> = {
    postState: fromPost.reducer,
};

export const initialState: AppState = {
    postState: fromPost.initialPostState,
}

export function getInitialState(){
    return {...initialState};
}

// export function logger(reducer:ActionReducer<any>): ActionReducer<any> {
//     return function(state,action) {
//         console.log('state',state);
//         console.log('action',action);

//         return reducer(state,action);
//     };
// }

// export const metaReducers = [logger];

export const getPostState = (state:AppState) => state.postState;