import {Post} from '../models/post';

import * as PostAction from '../actions/post.action';


export type PostActionType = PostAction.All;

type AllActionTypes = PostActionType;

export type PostState = Post;

export interface State{
    post: PostState;
}

export const initialPostState = {
    post: new Post(),
};

export function reducer(state: any = initialPostState,action: AllActionTypes):State {
    switch (action.type) {
        case PostAction.EDIT_POST:        
        return Object.assign({},state,{post:Object.assign({},state.post,{postMessage:action.payload})})
        case PostAction.LIKE:            
            return Object.assign({},state,{post:Object.assign({},state.post,{likes:++state.post.likes})})
        case PostAction.DISLIKE:
        return Object.assign({},state,{post:Object.assign({},state.post,{likes:--state.post.likes})})
        case PostAction.RESET:
            return initialPostState;
        default:
            return state;
    }    
}