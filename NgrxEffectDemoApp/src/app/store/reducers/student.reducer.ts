// import {Action} from '@ngrx/store';
import * as studentActions from '../actions/student.action'
import { Student } from '../models/student.model';


export type Action = studentActions.All;

export type State = StudentState;

export interface StudentState{
    student:Student,
    studentList:Student[]
}

export const InitialState:StudentState = {
    student : null,
    studentList : null,
}

export function reducer(state:any = InitialState,action:Action){
    switch(action.type){
        // case studentActions.STUDENTS_LIST:
        //     return Object.assign({},state,action.payload)
        case studentActions.STUDENTS_LIST_SUCCESS:
            return Object.assign({},state,{studentList:action.payload});
        // case studentActions.STUDENTS_LIST_ERROR:
        //     return Object.assign({},state,action.payload);

        // case studentActions.STUDENT_DETAILS:
        //     return Object.assign({},state,action.payload);
        case studentActions.STUDENT_DETAILS_SUCCESS:
            return Object.assign({},state,action.payload);
        // case studentActions.STUDENT_DETAILS_ERROR:
        //     return Object.assign({},state,action.payload);

        // case studentActions.STUDENT_ADD:
        //     return Object.assign({},state,action.payload);
        case studentActions.STUDENT_ADD_SUCCESS:
            return Object.assign({},state,action.payload);
        // case studentActions.STUDENT_ADD_ERROR:
        //     return Object.assign({},state,action.payload);
    }
}