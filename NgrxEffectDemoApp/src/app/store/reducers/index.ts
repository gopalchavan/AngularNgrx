import { ActionReducer, ActionReducerMap} from '@ngrx/store';


import * as fromStudent from './student.reducer';

export interface AppState {
    studentState: fromStudent.State;    
}

export const reducers: ActionReducerMap<AppState> = {
    studentState: fromStudent.reducer,
};

export const initialState: AppState = {
    studentState: fromStudent.InitialState,
}

export function getInitialState(){
    return {...initialState};
}

// export function logger(reducer:ActionReducer<any>): ActionReducer<any> {
//     return function(state,action) {
//         console.log('state',state);
//         console.log('action',action);

//         return reducer(state,action);
//     };
// }

// export const metaReducers = [logger];

export const getStudentState = (state:AppState) => state.studentState;