import {Action} from '@ngrx/store';

import {Student} from '../models/student.model';

export const STUDENTS_LIST          = '[student] list';
export const STUDENTS_LIST_SUCCESS  = '[student] list success';
export const STUDENTS_LIST_ERROR  = '[student] list error';

export const STUDENT_DETAILS        = '[student] detail';
export const STUDENT_DETAILS_SUCCESS = '[student] detail success';
export const STUDENT_DETAILS_ERROR = '[student] detail error';

export const STUDENT_ADD = '[student] add';
export const STUDENT_ADD_SUCCESS = '[student] add success';
export const STUDENT_ADD_ERROR = '[student] add error';

// export const STUDENT_DELETE = '[student] delete'
// export const STUDENT_DELETE_SUCCESS = '[student] delete success'
// export const STUDENT_DELETE_ERROR = '[student] delete error'


export class StudentList implements Action{
    readonly type = STUDENTS_LIST;
}
export class StudentListSuccess implements Action{
    readonly type = STUDENTS_LIST_SUCCESS;
    constructor(public payload:Student[]){}
}
export class StudentListError implements Action{
    readonly type = STUDENTS_LIST_ERROR;
    constructor(public payload:any){}
}


export class AddStudent implements Action{
    readonly type = STUDENT_ADD;
    constructor(public payload:any){}
}
export class AddStudentSuccess implements Action{
    readonly type = STUDENT_ADD_SUCCESS;
    constructor(public payload:any){}
}
export class AddStudentError implements Action{
    readonly type = STUDENT_ADD_ERROR;
    constructor(public payload:any){}
}

export class StudentDetails implements Action{
    readonly type = STUDENT_DETAILS;
    constructor(public payload:any){}
}
export class StudentDetailsSuccess implements Action{
    readonly type = STUDENT_DETAILS_SUCCESS;
    constructor(public payload:any){}
}
export class StudentDetailsError implements Action{
    readonly type = STUDENT_DETAILS_ERROR;
    constructor(public payload:any){}
}

export type All = StudentList
| StudentListSuccess
| StudentListError
| StudentDetails
| StudentDetailsSuccess
| StudentDetailsError
| AddStudent
| AddStudentSuccess
| AddStudentError