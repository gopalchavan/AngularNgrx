import {Effect,Actions} from '@ngrx/effects';
import {Injectable} from '@angular/core';
import * as StudentActions from '../actions/student.action';

import {StudentService} from '../../shared/services/student.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class StudentEffects{

    constructor(private _actions$:Actions,private _studentService:StudentService){}

    @Effect() students$ = this._actions$
    .ofType(StudentActions.STUDENTS_LIST)
    .switchMap(() =>
        this._studentService
        .studentList()
        .map(res=>{
            if(res){
                return new StudentActions.StudentListSuccess(res)
            }
        })
        .catch((err)=>Observable.of(new StudentActions.StudentListError(err)))
    )
}