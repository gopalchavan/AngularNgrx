import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';

import { AppComponent } from './app.component';

import {StudentService} from './shared/services/student.service';

import {reducers,getInitialState} from './store/reducers'

import {StudentEffects} from './store/effects/student.effect'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    StoreModule.forRoot(reducers,{
      initialState: getInitialState
    }),
    EffectsModule.forRoot([StudentEffects]),
  ],
  providers: [StudentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
