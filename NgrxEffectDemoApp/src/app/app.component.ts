import { Component } from '@angular/core';
import 'rxjs/Rx';

import {Store} from '@ngrx/store';

import * as RootReducer from './store/reducers'

import * as StudentActions from './store/actions/student.action';
import { Actions } from '@ngrx/effects';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  student$:any;

  constructor(private _store:Store<RootReducer.AppState>,private _actions$:Actions){
    this.student$ = this._store.select(RootReducer.getStudentState);
    this.listener();    
  }  

  private listener(){
    this._actions$.ofType(StudentActions.STUDENTS_LIST_ERROR)
    .map(res=>this.handleError(res));
    
    this._actions$.ofType(StudentActions.STUDENTS_LIST_SUCCESS)
    .map(res=>this.handleSuccess(res));

    this._store.dispatch(new StudentActions.StudentList());
  }

  handleError(result){
    console.log(result);
  }

  handleSuccess(res){
    console.log(res);
  }
}
