import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

import {environment} from '../../../environments/environment';

import {Student} from '../../store/models/student.model';

@Injectable()
export class StudentService {

  constructor(private http:Http) { }


  studentList(){
    var res = this.http.get(`${environment.baseUrl}/students`).map(res => res.json());
    return res;
  }

  addStudent(student:Student){
    return this.http.post(`${environment.baseUrl}/students`,student);
  }

  student(id:number){
    return this.http.get(`${environment.baseUrl}/students/id`).map(res=>res.json());
  }

}
