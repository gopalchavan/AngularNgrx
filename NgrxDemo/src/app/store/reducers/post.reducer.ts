import * as postactions from '../actions/post.actions';
import {post} from '../models/post.model';

export type Action = postactions.all

const defaultState:post = {
    text:"default text",
    likes:0
}

const newState = (state,newData)=>{
    return Object.assign({},state,newData);
}

export function postReducer(state:post = defaultState,action:Action){
    debugger;
    switch(action.type){
        case postactions.EDIT_TEXT:
            return newState(state,{text:action.payload});
        case postactions.UPVOTE:
            return newState(state,{likes:state.likes+1});
        case postactions.DOWNVOTE:
            return newState(state,{likes:state.likes-1});
        case postactions.RESET:
            return defaultState;
        default:
            return state;
    }

}