import {Action} from '@ngrx/store';

export function simpleReducer(state:string = "default Msg",action:Action){
    switch(action.type){
        case 'ENGLISH':
            return state = "Hello";
        case 'HINDI':
            return state = "Namastay";
        default:
            return state;
    }
}