import { Component } from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {post} from './store/models/post.model';
import * as Postactions from "./store/actions/post.actions";

interface Appstate{
  // message:string;
  post:post
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // message$:Observable<string>
  rs:any;
  text:string = "";
  post$:Observable<post>
  constructor(private store:Store<Appstate>){
    // this.message$ = this.store.select("message");
    this.post$ = this.store.select("post");
    // this.post$.subscribe(res=>this.rs = res);
    // console.log(this.rs);
    // console.log(this.post$);
  }
  // HindiMessage(){    
  //   this.store.dispatch({type:"HINDI"})
  //   this.meth();
  // }
  // EnglishMessage(){
  //   this.store.dispatch({type:"ENGLISH"})
  //   this.meth();
  // }
  private meth(){
    console.log(this.post$);
  }
  upvote(){
    this.store.dispatch(new Postactions.Upvote())
    this.meth();
  }
  downvote(){
    this.store.dispatch(new Postactions.Downvote());
    this.meth();
  }
  editText(){
    this.store.dispatch(new Postactions.EditText(this.text));
    this.meth();
  }
  resetPost(){
    this.store.dispatch(new Postactions.Reset());
    this.meth();
  }
}