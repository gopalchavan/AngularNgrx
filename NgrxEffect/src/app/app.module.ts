import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';

import {PostService} from './shared/services/post.service';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { postReducer } from './store/Reducers/post.reducer';
import { PostEffect } from './store/Effects/post.effect';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    EffectsModule.forRoot([PostEffect]),
    StoreModule.forRoot({
      post: postReducer
    }),
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
