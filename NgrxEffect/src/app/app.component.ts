import { Component } from '@angular/core';
import { Store }        from '@ngrx/store';
import { Observable }   from 'rxjs/Observable';

import { Post }         from './store/Models/post.model';
import * as postActions from './store/Actions/post.action'
// import 'rxjs/Rx'
interface AppState {
  post: Post;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  post$: Observable<Post>;
  constructor(private store: Store<AppState>) {
    this.post$ = this.store.select('post');
  }
  getPost() {
    this.store.dispatch(new postActions.GetPost(""));
  }
  // vote(post: Post, val: number) {
  //   this.store.dispatch(new postActions.UpdatePost({ post, val }));
  // }
}
