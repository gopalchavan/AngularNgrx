import {Action} from '@ngrx/store';
import {Post} from '../Models/post.model';

export const GET_POST = '[post] get';
export const GET_POST_SUCCESS = '[post] get success';
export const UPDATE_POST = '[post] update';
export const UPDATE_POST_SUCCESS = '[post] update success';
export const UPDATE_POST_FAIL = '[post] update fail';

export class GetPost implements Action{
    readonly type = GET_POST
    constructor(public payload:string){}
}

export class GetPostSuccess implements Action{
    readonly type = GET_POST_SUCCESS
    constructor(public payload:Post){}
}

export class UpdatePost implements Action{
    readonly type = UPDATE_POST;
    constructor(public payload:any){}
}

export class UpdatePostSuccess implements Action{
    readonly type = UPDATE_POST_SUCCESS;
    constructor(public payload:any){}
}

export class UpdatePostFail implements Action{
    readonly type = UPDATE_POST_FAIL;
    constructor(public payload:any){}
}

export type All = GetPost
| GetPostSuccess
| UpdatePost
| UpdatePostSuccess
| UpdatePostFail;
