import * as PostActions from '../Actions/post.action';
import {Post} from '../Models/post.model';

export type Action = PostActions.All;

export const InitialState : Post= {
    title:"default",
    likes: 0
}

export function postReducer(state:Post = InitialState,actions:Action){
    switch(actions.type){
        case PostActions.GET_POST:
            return Object.assign({},state);
        case PostActions.GET_POST_SUCCESS:
            return Object.assign({},state,actions.payload);
        case PostActions.UPDATE_POST:
            return Object.assign({},state,actions.payload); 
        case PostActions.UPDATE_POST_SUCCESS:
            return Object.assign({},state);
        case PostActions.UPDATE_POST_FAIL:
            return Object.assign({},state,actions.payload);
    }
}