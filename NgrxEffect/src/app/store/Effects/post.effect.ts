import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import * as PostAction from '../Actions/post.action';
import { Observable } from 'rxjs/Observable';
import {map,filter} from 'rxjs/operators';
import "rxjs/Rx";

import { Post } from '../Models/post.model'

import { PostService } from '../../shared/services/post.service';
import { GET_POST_SUCCESS } from '../Actions/post.action';

export type Action = PostAction.All;


@Injectable()
export class PostEffect {
    constructor(private actions: Actions, private postService: PostService) { }
    
    @Effect()
    getPost: Observable<Action> = this.actions
    .ofType<PostAction.GetPost>(PostAction.GET_POST)
    .map(action => action.payload)
    .switchMap((res)=> this.postService
        .post()                
        .map(res => new PostAction.GetPostSuccess(res))

    
}